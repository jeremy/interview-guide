# Interview Guide with Jeremy
Hi! If you're reading this, you probably have an interview with me. Thanks a lot for making some time; I'm 
super excited to talk with you and learn more about you. 

This document is intended to set some context and expectations for our conversation. I know that interviews
can sometimes be stressful, so my goal here is to arm you with as much information as I can to make you feel at ease.
* If you haven't read my personal README, it might help you learn a little about me and my style: https://gitlab.com/jeremy/readme/-/blob/master/README.md

# Before our conversation
* If you need any additional accommodations prior to our conversation, just email and ask. There is absolutely, 100% no ill consequence to rescheduling our conversation or asking for more time; I totally understand if family duties or something unexpected is calling for your attention.
* If you are differently abled, please educate me on how I can support you during our conversation.
* Our conversation will likely be over Zoom. Please take some time to test your setup beforehand.
* Please do your best to show up on time. Rescheduling a conversation is absolutely fine, but a no-show without notice does frustrate me. If you're late, I'll check my email and stay on the call for 5-10 minutes, then depart.

# During our conversation
* With every formal conversation I have, I create a private notes document to chronicle specific moments. If you hear me typing, it's not because I'm answering email or am on Slack - you have my undivided attention, I'm simply taking notes.
* I'm trying to get the best out of you and make you feel as comfortable as possible. I believe in low-pressure conversations that help provide me with your best thinking; there are no trick questions or "points off" if you take a moment to think or need to ask me to clarify.
* To make sure we have an efficient conversation, I may interrupt you. Thanks for your patience with this; this isn't a signal that something is "wrong", I just want to make sure we cover everything I'd like to understand.
* We'll always leave time for your questions at the end of the conversation. I don't evaluate the questions you ask, so please ask unfiltered questions that you _actually_ want the answers to. Interviewing is a two-way street, and it's just as important that this is a fit for you as it is the other way around.

# After our conversation
* Please don't feel any obligation whatsoever to send me a thank-you note. I think generic thank-you notes are formalities with no real value; you've already dedicated enough time and energy to our conversation. Thanks for the thought, but don't worry about it!
  * However, _please_ feel free to follow up if you'd like to emphasize something I should know, something important you forgot to mention, or want to follow up on something we talked about: jeremy at remote dot com.
* If I decline to move you forward in the process: 
  * Please do not interpret this outcome as a failure or that you did something "wrong". I'm looking for a fit in both directions, and I failed to see a strong mutual connection in the short time we had together. I want to be able to support you in your career and help you find a place where you can thrive, but every company and opportunity will require something different.
  * While decisions are final, I'm happy to schedule feedback (sync call or async via Loom) for your learning and development. Please reach out to me at jeremy at remote dot com.

Thank you again for taking some time to talk with me. I really appreciate you and your time - looking forward to our conversation! 🙌
